package com.growright.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: HelloController
 * Description:
 *
 * @Author YH Su
 * @Create 16/8/2023 2:42 pm
 * @Version 1.0
 */

@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        System.out.println("sdf");
        return "sdf";
    }


}
