# Grow Right
[![growright](./logo.png)](https://www.growright.digital/)


## Project Overview
Grow Right is a revolutionary new job search information service platform that matches job searchers based on their talents and preferences. Job seekers only need to build a profile on the Grow Right platform, which includes a CV, wage expectations, and career goals, to receive job listings for relevant employment.

In terms of technological implementation, Grow Right members filter job applicants' information by keywords to match the best candidates with open positions using machine learning technology and natural language processing models.


## Project Purpose
Our work this semester is mainly divided into three parts: front-end page design, back-end architecture construction and algorithm design.



## Team Chart
| Name        | UID      | Role                | Part              |
|-------------|----------|---------------------|-------------------|
| Bryce  | -        | Client              | -                 |
| Peiyuan Yu    |u7543592   | Spokesperson | Front-end |
| Zixin Su  | u7425489   | Spokesperson | Back-end |
| Zhoushu He  | u5926302  | Member              |        Algorithm  |
| Bowen Hu    | u7152919  | Member              |          Back-end |
| Johnny Wang  | u7543414  | Member              |         Front-end   |
| Boyi Zhang   | u7340957  | Member              |          Algorithm  |
| Guanghao Li       |   u7560865       |   Member|        Front-end  |

##Local Development
   ###Frontend
```bash
cd frontend
```
```bash
yarn install
yarn dev
```

## Task Management
- [Risk Log](https://trello.com/b/aSVLRHMx/risk-log)
- [Decision Log](https://trello.com/b/aD2hFgNx/decision-log)
- [Reflection Log](https://trello.com/b/WmeNxHQf/feedback-log)
- [Workflow](https://trello.com/b/QAL5masH/careermatch2023s2)

##   Conflict Management
```mermaid
graph TD
    A[Acknowledge Conflict] --> B[Encourage Open Communication]
    B --> C[Identify Root Cause]
    B --> D[Listen Actively]
    B --> E[Maintain Neutrality]
    B --> F[Collaborate on Solutions]
    F --> G[Reach Mutual Agreement]
    B --> H[Establish Clear Expectations]
    B --> I[Implement Agreed-Upon Solution]
    I --> J[Monitor Progress & Adjust]
    B --> K[Follow-up]
    K --> L[Ensure Successful Resolution]
    B --> M[Learn from Experience]
```

##  Audit 1
- [Audit 1 slides](https://docs.google.com/presentation/d/1FCf-vvxfe3IwEWnyMZ2CNGmFyumX6I3H/edit?usp=drive_link&ouid=112380585913478941026&rtpof=true&sd=true)
- [Statement of Work](./Documentation/SOW-CareerMatch.pdf)
- [Constraint](./Documentation/Resource___Risk___Potential_Cost___Constraint.pdf?ref_type=heads)
- [Potential Cost](./Documentation/Potential_Cost_.pdf?ref_type=heads)
- [Project Client Map](./Documentation/Project_Client_Map_GrowRight.docx.pdf?ref_type=heads)


##  Audit 2

##  Audit 3


##  Meeting Minutes
### Tutorial Meeting
https://trello.com/b/g7zCs1rj/meetingtotorial

### Client Meeting
https://trello.com/b/fEZeGvFK/meetingclient

### Group Meeting
https://trello.com/b/E3BCqqb5/meetinggroup


##  Repository
[GitLab](https://gitlab.com/hideontree/grow_right)

[Google Drive](https://docs.google.com/document/d/1IcZrLrrOLNap4V3OZN67SxIv0MTp2mpW/edit?usp=drive_link)

##  Documentation
- [Statement of Work](./Documentation/SOW-CareerMatch.pdf)
- [Team Charter](./Documentation/Team_Charter.pdf)
