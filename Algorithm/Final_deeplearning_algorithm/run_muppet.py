
import torch
import pandas as pd
from torch.utils.data import Dataset, DataLoader, random_split
from importlib import import_module
import torch
from torch.utils.data import Dataset, DataLoader
from transformers import AlbertTokenizer, AlbertForSequenceClassification, AdamW, get_linear_schedule_with_warmup
import warnings
from transformers import RobertaForSequenceClassification, RobertaTokenizer
warnings.filterwarnings("ignore", message="Be aware, overflowing tokens are not returned.*")

from sklearn.metrics import accuracy_score, precision_recall_fscore_support
import pandas as pd
import numpy as np
from preprocess import *
from transformers import AlbertConfig, AlbertForSequenceClassification
def main():

    x = import_module('models.' + 'Albert')
    config = x.Config()

    model = RobertaForSequenceClassification.from_pretrained("facebook/muppet-roberta-base", num_labels=2).to(config.device)
    tokenizer = RobertaTokenizer.from_pretrained("facebook/muppet-roberta-base")
    job_descs, user_resumes, labels = read_csv(config)
    data = construct_data(job_descs, user_resumes, labels)
    train_df, val_df = split_dataset(data)

    # dataset
    train_dataset = JobRecommendationDataset(config, train_df, tokenizer)
    val_dataset  = JobRecommendationDataset(config, val_df, tokenizer)
    # dataloader
    train_dataloader = DataLoader(train_dataset, batch_size=config.batch_size, shuffle=True)
    val_dataloader = DataLoader(val_dataset, batch_size=config.batch_size, shuffle=False)

    # initialize
    optimizer = AdamW(model.parameters(), lr=config.lr)

    num_training_steps = len(train_dataloader) * config.num_epochs
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=int(0.1 * num_training_steps),
                                                num_training_steps=num_training_steps)
    best_val_f1 = 0
    no_improvement_epochs = 0
    train_losses = []
    val_losses = []
    train_f1_scores = []
    val_f1_scores = []


    #todo
    for epoch in range(config.num_epochs):
        train_loss, train_f1, train_cm = train_2(config, model, train_dataloader, optimizer, scheduler)

        val_loss, val_f1 = evaluate(config, model, val_dataloader, optimizer)

        print(f"Epoch: {epoch + 1}/{config.num_epochs}")
        print(f"Train Loss: {train_loss:.4f} | Train F1: {train_f1:.4f}")
        print(f"Test Loss: {val_loss:.4f} | Val F1: {val_f1:.4f}")

        train_losses.append(train_loss)
        val_losses.append(val_loss)
        train_f1_scores.append(train_f1)
        val_f1_scores.append(val_f1)
        if val_f1 > best_val_f1:
            best_val_f1 = val_f1
            no_improvement_epochs = 0
            torch.save(model.state_dict(), "./save_dict/muppet_best_model.pt")
            print("Saved best model.")
        else:
            no_improvement_epochs += 1

        if no_improvement_epochs >= 30:
            print("No improvement in test F1 for 10 consecutive epochs. Stopping training.")
            break
    return train_losses, val_losses, train_f1_scores, val_f1_scores
import matplotlib.pyplot as plt
if __name__ == '__main__':
    train_losses, val_losses, train_f1_scores, val_f1_scores= main()

    plt.figure()
    plt.plot(train_losses, label='Train Loss')
    plt.plot(val_losses, label='Val Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()
    plt.title('Loss over Epochs')
    plt.savefig('./save_dict/muppet_loss_plot.png')
    plt.show()


    plt.figure()
    plt.plot(train_f1_scores, label='Train F1')
    plt.plot(val_f1_scores, label='Val F1')
    plt.xlabel('Epoch')
    plt.ylabel('F1 Score')
    plt.legend()
    plt.title('F1 Score over Epochs')
    plt.savefig('./save_dict/muppet_f1_plot.png')
    plt.show()
    #
    # # recommend
    # x = import_module('models.' + 'Albert')
    # config = x.Config()
    # job_descs, user_resumes, labels = read_csv(config)
    # data = construct_data(job_descs, user_resumes, labels)
    # job_descriptions = data['job descriptions'].tolist()
    # best_model = RobertaForSequenceClassification.from_pretrained("facebook/muppet-roberta-base", num_labels=2)
    # best_model.load_state_dict(torch.load("./save_dict/muppet_best_model.pt"))
    # best_model.to(config.device)
    # tokenizer = RobertaTokenizer.from_pretrained("facebook/muppet-roberta-base")    # user_profile = ' I am a full-stack web developer with over 5 years of experience in building responsive and dynamic web applications. I am skilled in several programming languages and frameworks, including JavaScript, React, Node.js, and Ruby on Rails. I am passionate about using technology to solve complex problems and deliver high-quality user experiences.'
    # # user_profile = 'Business development and client relationship management; specifically focusing on driving market/client penetration of ' \
    # #                'technology solutions complementing Quintiles core products and services to the BioPharma industry. ' \
    # #                'Global scope, and oversight of a client portfolio with a total value of $100M.'
    #
    # user_profile = "Sarah is an experienced Project Manager with over 7 years of experience in managing complex projects in various industries. She has a proven track record of delivering projects on time, within budget, and meeting the client's expectations.Her expertise lies in client relationship management, where she has consistently demonstrated her ability to build and maintain strong relationships with clients, ensuring their satisfaction with the project outcomes. She has excellent communication skills and can communicate effectively with stakeholders at all levels."
    # # job_descriptions = ["Job description 1", "Job description 2", "Job description 3"]
    # matched_jobs = recommend(config, best_model, tokenizer, user_profile, job_descriptions)
    # for idx, job in enumerate(matched_jobs[:5]):
    #     print('recommend job ' + str(idx) +':', job)
    #     print('*****************************************************************************')