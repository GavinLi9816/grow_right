from importlib import import_module
import pandas as pd
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader, random_split
from transformers import AutoTokenizer, AutoModel, AdamW, get_linear_schedule_with_warmup
from preprocess_user import *
from preprocess_job import *
import random
from sentence_transformers import SentenceTransformer, util
import torch
import time


"""
Use sbert to extract features.
Use bert as baseline
Use clustering algorithm to generate positve and negative sample
"""

def sbert_embedding(model, profiles, desc):
    profiles_embedding, job_desc_embeddings = model.encode(profiles), model.encode(desc)
    return profiles_embedding, job_desc_embeddings


class ResumeJobDataset(Dataset):
    def __init__(self, resumes, job_descriptions, augmented_job_descriptions):
        self.resumes = resumes
        self.job_descriptions = job_descriptions
        self.augmented_job_descriptions = augmented_job_descriptions

    def __len__(self):
        return len(self.resumes)

    def __getitem__(self, idx):
        return self.resumes[idx], self.job_descriptions[idx], self.augmented_job_descriptions[idx]


# sampling use kmeans
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import cosine_similarity

def select_samples(resume, job_descs, kmeans, sbert_model):
    """

    :param resume: a user profile
    :param job_descs: all of the job descriptions
    :param kmeans:
    :param sbert_model:
    :return:
    """
    resume_embedding = sbert_model.encode([resume])[0]
    job_desc_embeddings = sbert_model.encode(job_descs)
    similarities = cosine_similarity([resume_embedding], job_desc_embeddings)
    # find cluster result
    cluster_id = np.argmax(similarities)

    # select neg and pos
    positive_samples = [jd for i, jd in enumerate(job_descs) if kmeans.labels_[i] == cluster_id]
    negative_cluster_ids = [i for i in range(config.num_clusters) if i != cluster_id]
    negative_samples = [jd for i, jd in enumerate(job_descs) if kmeans.labels_[i] in negative_cluster_ids]

    return positive_samples, negative_samples


if __name__ == '__main__':
    x = import_module('models.' + 'siamese')
    config = x.Config()

    # construct dataset
    education, interest, skill = read_csv(config)
    selected_features = combine_features(education, interest, skill)
    cleaned_profiles, cleaned_jobs = preprocess(config)  # list

    # embeddings
    sbert = SentenceTransformer(config.sbert_name)
    profiles_embedding, job_desc_embeddings = sbert_embedding(sbert, cleaned_profiles, cleaned_jobs)
    a = 1



    #
    # # training
    # for epoch in range(config.num_epochs):
    #     st = time.time()
    #     training_loss = train_one_epoch(config, model, train_dataloader, optimizer, scheduler)
    #     end = time.time()
    #     test_loss = 0
    #     print('epoch {}/{}, training loss: {:.3f}, test loss: {:.3f}, training time: {:.3f}'.format(epoch, config.num_epochs, training_loss, test_loss, end-st))
