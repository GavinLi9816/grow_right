import pandas as pd
from importlib import import_module
import os

def read_job(config):
    df = pd.read_csv(config.job_description)
    df = df.fillna(' ', inplace=True)
    return df

def concatenate_feature(row):
    return ','.join([str(x) for x in row[1:]])

def combine_features_job(df, config):
    if os.path.exists(config.merge_job_path):
        selected_job_features = pd.read_csv(config.merge_job_path)
        print('job description merged')
    else:
        print('build job description')
        selected_job_features = df[config.job_features]

        selected_job_features['job description'] = df.apply(concatenate_feature, axis=1)

        selected_job_features = selected_job_features.loc[:, ['url', 'job description']]
        selected_job_features.to_csv(config.merge_job_path, index=False)



if __name__ == '__main__':
    x = import_module('models.' + 'Bert')
    config = x.Config()
    job_description =read_job(config)

    combine_features_job(job_description, config)