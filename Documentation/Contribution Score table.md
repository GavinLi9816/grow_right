# Week 4

| item                                              | Zixin Su | Peiyuan Yu | Bowen Hu | Boyi Zhang | Jiangning Wang | Guanghao Li | Zhoushu He |
|---------------------------------------------------|:--------:|:----------:|:--------:|:----------:|:--------------:|:-----------:|:----------:|
| Attend client meeting - 0.11                      |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Attend tutorial meeting - 0.11                    |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Attend group meeting - 0.11                       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with client - 0.11                    |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with tutor - 0.11                     |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with shadow team - 0.11               |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Represent agenda in meeting - 0.33                |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Write down thought on own initiative - 0.5        |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Complete job after distribute in meetings - 0.5   |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Own distributed job and reflected in Gitlab - 0.5 |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Positive in group chat and discussion - 0.8       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Come up with own idea - 0.2                       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Bonus for everyone - 1                            |   [x]    |    [x]     |   [x]    |    [x]     |      [x]       |     [x]     |    [x]     |
| Total score                                       |          |            |          |            |                |             |            |


# Week 5

| item                                              | Zixin Su | Peiyuan Yu | Bowen Hu | Boyi Zhang | Jiangning Wang | Guanghao Li | Zhoushu He |
|---------------------------------------------------|:--------:|:----------:|:--------:|:----------:|:--------------:|:-----------:|:----------:|
| Attend client meeting - 0.11                      |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Attend tutorial meeting - 0.11                    |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Attend group meeting - 0.11                       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with client - 0.11                    |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with tutor - 0.11                     |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with shadow team - 0.11               |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Represent agenda in meeting - 0.33                |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Write down thought on own initiative - 0.5        |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Complete job after distribute in meetings - 0.5   |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Own distributed job and reflected in Gitlab - 0.5 |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Positive in group chat and discussion - 0.8       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Come up with own idea - 0.2                       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Bonus for everyone - 1                            |   [x]    |    [x]     |   [x]    |    [x]     |      [x]       |     [x]     |    [x]     |
| Total score                                       |          |            |          |            |                |             |            |


# Week 6

| item                                              | Zixin Su | Peiyuan Yu | Bowen Hu | Boyi Zhang | Jiangning Wang | Guanghao Li | Zhoushu He |
|---------------------------------------------------|:--------:|:----------:|:--------:|:----------:|:--------------:|:-----------:|:----------:|
| Attend client meeting - 0.11                      |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Attend tutorial meeting - 0.11                    |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Attend group meeting - 0.11                       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with client - 0.11                    |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with tutor - 0.11                     |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with shadow team - 0.11               |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Represent agenda in meeting - 0.33                |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Write down thought on own initiative - 0.5        |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Complete job after distribute in meetings - 0.5   |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Own distributed job and reflected in Gitlab - 0.5 |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Positive in group chat and discussion - 0.8       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Come up with own idea - 0.2                       |   [ ]    |    [ ]     |   [ ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Bonus for everyone - 1                            |   [x]    |    [x]     |   [x]    |    [x]     |      [x]       |     [x]     |    [x]     |
| Total score                                       |          |            |          |            |                |             |            |

